<?php
require_once 'engine/utils.php';
require_once 'engine/config.php';

$count=1;
$currentIndex = $_GET['pos'];
$fileset=get_files(array($SITE_EXT_DIRS['webm']), 'webm', $count, $currentIndex);
$nextPage = $_SERVER['REQUEST_URL'].($fileset[1] ?  '?pos='.($currentIndex + $count) : "#");
$prevPage = $_SERVER['REQUEST_URL'].($currentIndex > 0 ? '?pos='.($currentIndex - $count) : "#");

$PAGE_BODY = '';
foreach($fileset[0] as $filename) {
	$PAGE_BODY .= "<figure>".
                  "<video controls=\"controls\" width=\"100%\" poster=\"$SITE_PREFIX/img/bg.png\">".
                  "<source src=\"$SITE_PREFIX/getfile.php?filename=$filename\" type='video/webm; codecs=\"vp8, vorbis\"' />".
                  "</video>".
                  "<a href=\"$SITE_PREFIX/getfile.php?filename=$filename\" target=\"_blank\">".
				  "<figcaption>Видео: $filename</figcaption></figure></a>";


}
$PAGE_BODY .= "
        <script>
        \$(document).ready(function() {    // ''.
            var hum = new Hammer(\$('section' )[ 0 ], { domEvents: true } );
            hum.on('swiperight', function() { window.location.replace(window.location.pathname + '$prevPage') } );
            hum.on('swipeleft', function() { window.location.replace(window.location.pathname + '$nextPage'); });
        });
        </script> ";
$WINDOW_TITLE = "Сайт наших свадебных фото и видео";
$PAGE_TITLE = "Видео №".($currentIndex + 1);
$PAGE_NAV = array(
	0 => array("address" => $prevPage, "text" => "<img src=\"$SITE_PREFIX/img/prev.png\" width=\"24px\" />"),
	1 => array("address" => '/', "text" => "<img src=\"$SITE_PREFIX/img/home.png\" width=\"24px\" />"),
	2 => array("address" => $nextPage, "text" => "<img src=\"$SITE_PREFIX/img/next.png\" width=\"24px\" />")
);

$PAGE_FOOTER = "";
require 'engine/auth.php';
require 'engine/template.php';
?>