<?php
require 'auth.php';
require_once 'config.php';

$ext = pathinfo($_GET['filename'], PATHINFO_EXTENSION);

$base_path=$SITE_EXT_DIRS[$ext];

$name = $base_path.$_GET['filename'];

if(!file_exists($name)) {
    header("HTTP/1.0 404 Not Found");
	echo "File not found: ".$name;
    exit;
}

header('Content-Description: File Transfer');
header('Content-Type: '.$SITE_EXT_MIMES[$ext]);
header('Content-Disposition: attachment; filename='.$_GET['filename']);
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: '.filesize($name));
readfile($name);

?>