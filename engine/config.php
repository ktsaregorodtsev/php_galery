<?php
//! @brief Отображается при отсутствии в странице/
$DEFAULT_TITLE_PAGE ='Site name';
//! @brief Префикс для построения абсолютного пути
$SITE_PREFIX = "";
$SITE_LOGIN_PAGE = 'login.php';
$SITE_PASSWORD = '*****';
$SITE_EXT_DIRS=array('webm' => '../video/', 'jpg' => '../photo/');
$SITE_EXT_MIMES=array('webm' => 'video/webm', 'jpg' => 'image/jpeg');
?>