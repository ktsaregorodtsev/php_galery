<?php
require_once 'config.php';
require_once 'utils.php';
if(!isset($WINDOW_TITLE)) throw new Exception("\$WINDOW_TITLE not found");
if(!isset($PAGE_TITLE)) throw new Exception("\$PAGE_TITLE not found");
if(!isset($PAGE_BODY)) throw new Exception("\$PAGE_BODY not found");
if(!isset($PAGE_FOOTER)) throw new Exception("\$PAGE_FOOTER not found");
?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<?php if(isMobile()) { ?><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"><?php } ?>

	<title><?php echo $WINDOW_TITLE; ?></title>
	<script type="text/javascript"><?php echo $PAGE_SCRIPT; ?></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"> </script>
    <script src="<?php echo $SITE_PREFIX;?>/js/hammer.min.js"> </script>
	<link rel="stylesheet" type="text/css" href="<?php echo $SITE_PREFIX; ?>/css/main.css" />
	<style type="text/css"><?php echo $PAGE_CSS; ?></style>
	
</head>
<body>
	<article>
		<header><?php echo $PAGE_TITLE; ?></header>
		<?php if(is_array($PAGE_NAV)) { ?>
		<nav><?php foreach($PAGE_NAV as $link) { ?><a href="<?php echo $link['address']; ?>"><?php echo $link['text']; ?></a> <?php }; ?></nav>
		<?php } ?>
		<section><?php echo $PAGE_BODY; ?></section>
		<?php if(is_array($PAGE_NAV)) { ?>
		<nav><?php foreach($PAGE_NAV as $link) { ?><a href="<?php echo $link['address']; ?>"><?php echo $link['text']; ?></a> <?php }; ?></nav>
		<?php } ?>
	</article>
	<footer><?php echo $PAGE_FOOTER; ?></footer>
</body>
</html>
